# OpenML dataset: teachingAssistant

https://www.openml.org/d/45688

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Dataset from the MLRR repository: http://axon.cs.byu.edu:5000/

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45688) of an [OpenML dataset](https://www.openml.org/d/45688). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45688/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45688/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45688/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

